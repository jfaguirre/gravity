const minJSON = require('jsonminify')

module.exports = {
  copyright: 'Atre',
  name: 'Webpack config',
  // Esta info la agregamos para disponibilizarla en las vistas
  info: {
    author: 'Atre',
    description: 'Card dynamics banco ripley',
    title: 'Gravity',
    url: 'https://www.bancoripley.cl',
    site_name: 'bancoripley.cl',
    analytics: ''
  },
  paths: {
    src: {
      base: './src/',
      scripts: './src/scripts/',
      styles: './src/styles/',
      images: './src/images/'
    },
    views: './src/views/',
    dist: {
      base: './dist/',
      scripts: './scripts/',
      styles: './styles/',
      images: './images/'
    }
  },
  entries: {
    // Se agregan en orden de aparición
    // los módulos de node se agregan así 'vue'
    vendor: ['./src/scripts/vendor.js'],
    app: './src/scripts/app.js'
  },
  pages: [
    'index'
  ],
  copyWebpackConfig: [
    {
      from: 'data/**/*.json',
      transform: content => {
        return minJSON(content.toString())
      }
    },
    {
      from: './app.yaml',
      to: './'
    }
  ],

  htmlLoaderConfig: {
    attrs: [':src', ':data-src']
  },

  workboxConfig: {
    swDest: './sw.js',
    exclude: [
      /\.(png|jpe?g|gif|svg|webp)$/i,
      /\.map$/,
      /^manifest.*\\.js(?:on)?$/,
    ],
    clientsClaim: true,
    skipWaiting: true
  },

  manifestConfig: {
    'name': 'Gravity',
    'short_name': 'Gravity',
    'background_color': '#222222',
    'display': 'standalone',
    'theme_color': '#01579b',
    'theme-color': '#000000',
    'start_url': './'
  },

  devServerConfig: {
    public: () => process.env.DEVSERVER_PUBLIC || 'http://localhost:8080',
    host: () => process.env.DEVSERVER_HOST || 'localhost',
    poll: () => process.env.DEVSERVER_POLL || false,
    port: () => process.env.DEVSERVER_PORT || 8080,
    https: () => process.env.DEVSERVER_HTTPS || false,
  },
}
